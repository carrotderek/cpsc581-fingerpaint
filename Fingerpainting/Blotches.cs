﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Effects;
using System.Windows.Threading;
using System.Windows.Media.Animation;

using FluidKit.Helpers.Animation;

namespace Fingerpainting
{
    class Blotches
    {
        // size of the ellipses ( x and y )
        private const int SIZE = 30;

        private Canvas canvas;
        private Path ellipse;
        private EllipseGeometry geometry;
        private PennerDoubleAnimation hoverAnimation;
        private DoubleAnimation blurAnimation;
        private DoubleAnimation fadeAnimation;
        private Storyboard blurIn = new Storyboard();
        private Storyboard hoverObj = new Storyboard();
        private Storyboard fadeOut = new Storyboard();

        public Boolean isBlurEffectEnabled = false;
        

        public Blotches(Canvas canvas, Window1 window, string key, float pressure, double x, double y, Color color)
        {

            this.canvas = canvas;

            geometry = new EllipseGeometry();
            ellipse = new Path();
            SolidColorBrush brush = new SolidColorBrush();

            BlurEffect blurEffect = new BlurEffect();
            blurEffect.Radius = 0;

            geometry.Center = new Point(x, y);
            geometry.RadiusX = (SIZE * pressure) / 50;
            geometry.RadiusY = (SIZE * pressure) / 50;

            brush.Color = color;
            ellipse.Fill = brush;
            ellipse.HorizontalAlignment = HorizontalAlignment.Center;
            ellipse.VerticalAlignment = VerticalAlignment.Center;
            ellipse.Effect = blurEffect;
            ellipse.Data = geometry;
            canvas.Children.Add(this.ellipse);

            this.ellipse.Loaded += new RoutedEventHandler(ellipse_Loaded);

            // init animation
            this.hoverAnimation = new PennerDoubleAnimation(
                            Equations.ExpoEaseIn,
                            0, 0,
                            new Duration(new TimeSpan(0, 0, 3)));



            NameScope.SetNameScope(ellipse, new NameScope());
            ellipse.RegisterName("ellipse", blurEffect);

            // init storyboard
            Storyboard.SetTargetProperty(hoverAnimation, new PropertyPath(Canvas.TopProperty));
            hoverObj.Children.Add(hoverAnimation);

            this.hoverAnimation.Completed += new EventHandler(hoverAnimation_Completed);
        }

        void ellipse_Loaded(object sender, RoutedEventArgs e)
        {
            this.hoverAnimation.From = 8;
            this.hoverAnimation.To = 30;
            this.hoverAnimation.Equation = Equations.ElasticEaseOut;
            this.hoverObj.Begin(ellipse);

        }

        void hoverAnimation_Completed(object sender, EventArgs e)
        {
            /*
            this.blurAnimation = new DoubleAnimation(0.0, 5.0, new Duration(TimeSpan.FromMilliseconds(50)));

            Storyboard.SetTargetName(blurAnimation, "ellipse");
            Storyboard.SetTargetProperty(blurAnimation, new PropertyPath(BlurEffect.RadiusProperty));
            blurIn.Children.Add(blurAnimation);

            blurIn.Begin(ellipse);*/

            this.fadeAnimation = new DoubleAnimation(100.0, 0.0, new Duration(new TimeSpan(0, 0, 5)));
            this.fadeAnimation.Completed += new EventHandler(fadeAnimation_Completed);
            Storyboard.SetTargetProperty(fadeAnimation, new PropertyPath(Path.OpacityProperty));
            fadeOut.Children.Add(fadeAnimation);
            fadeOut.Begin(ellipse);
        }

        void fadeAnimation_Completed(object sender, EventArgs e)
        {
            this.canvas.Children.Remove(this.ellipse);
        }
    }
}
