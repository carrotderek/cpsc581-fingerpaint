﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Windows.Media.Animation;

//for pressure sensitive keyboard
using PSK;

namespace Fingerpainting
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {

        private string[,] keyboardKeys = {
                                            { "`", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "-", "=", "back" },
                                            { "tab", "q", "w", "e", "r", "t", "y", "u", "i", "o", "p", "[", "]", "" },
                                            { "caps", "a", "s", "d", "f", "g", "h", "j", "k", "l", ";", "'", "ret", "" },
                                            { "shift", "z", "x", "c", "v", "b", "n", "m", ",", ".", "/", "rightshift", "", "" }
                                          };


        // these dictionaries will be used to map coordinates to draw on the canvas
        // mapping between keys and sitance between top-edge of element to the top of the canvas
        private Dictionary<string, double> xMapping = new Dictionary<string, double>();

        // mapping between keys and distance between left-edge of element to the left of the canvas
        private Dictionary<string, double> yMapping = new Dictionary<string, double>();

        // if the keyboard helper image is active or not (invoked by F1)
        private Boolean isKeyboardActive = false;
        
        // drawing on the canvas
        private Blotches blotches;
        
        // color of the blotch
        private Color color;

        private float pressure;
        private string key;

        private delegate void AnimateEllipseDrawn(string key, float pressure);

        PressureKeyboard pressKeyboard;

        public Window1()
        {
            InitializeComponent();
            InitializePressureKeyboard();
            InitializeDrawing();
        }

        /**
         * Will initialize mappings between keyboard and canvas
         **/
        private void InitializeDrawing()
        {
            //REFACTOR THIS SHIT. IT'S SERIOUS.
            double top = 0;
            double left ;
            string key = "";

            for (int j = 0; j <= 3; j++)
            {
                top += 90;
                left = 35;
                for (int i = 0; i <= 13; i++)
                {
                    key = this.keyboardKeys[j,i];

                    if (String.IsNullOrEmpty(key)) continue;

                    yMapping.Add(key, top);
                    xMapping.Add(key, left);
                    left += 80;
                }
            }
        }

        /**
         * Initialize the pressure keyboard 
         **/
        private void InitializePressureKeyboard()
        {
            this.pressKeyboard = new PressureKeyboard();
            // Register for the KeyPressed and KeyReleased events.
            this.pressKeyboard.KeyPressed += new EventHandler<KeyPressedEventArgs>(pressKeyboard_KeyPressed);
            this.pressKeyboard.KeyReleased += new EventHandler<KeyPressedEventArgs>(pressKeyboard_KeyReleased);
        }

        void pressKeyboard_KeyReleased(object sender, KeyPressedEventArgs e)
        {
        }

        void pressKeyboard_KeyPressed(object sender, KeyPressedEventArgs e)
        {
            setPressure(e.KeyPressed.Pressure);
            setKey(e.KeyPressed.Text.ToLower());
            
            // if the key is in the mapping, it should be drawn
            if (yMapping.ContainsKey(e.KeyPressed.Text.ToLower()))
                 this.DrawBlotch(e.KeyPressed.Text.ToLower(), e.KeyPressed.Pressure);

            // f1 to bring up keyboard image shown previously.
            if (e.KeyPressed.Text.ToLower().Equals("f1"))
                showKeyboard();

            if (e.KeyPressed.Text.ToLower().Equals("space"))
                eraseCanvas();
        }

        private void eraseCanvas()
        {
            theCanvas.Children.Clear();
        }

        /**
         * Brings up the keyboard layout, providing the user information of
         * where on the canvas is being drawn on corresponding to which key
         * 
         * Will disappear when key is hit again
         **/
        private void showKeyboard()
        {
            DoubleAnimation bgAnimation = new DoubleAnimation();
            bgAnimation.Duration = new Duration(TimeSpan.FromMilliseconds(40));

            if (!isKeyboardActive)
            {
                this.isKeyboardActive = true;

                bgAnimation.From = 0.0;
                bgAnimation.To = 60.0;
            }

            else
            {
                this.isKeyboardActive = false;

                bgAnimation.From = 60.0;
                bgAnimation.To = 0.0;
            }

            Storyboard.SetTargetName(bgAnimation, "keyboardImage");
            Storyboard.SetTargetProperty(bgAnimation, new PropertyPath(Image.OpacityProperty));
            bgStoryboard.Begin();
        }

        // this will draw a blotch when the key is pressed.
        // the more pressure is applied, the greater the circumference of the blotch. 
        private void DrawBlotch(string key, float pressure)
        {
            try
            {
                if ((xMapping.ContainsKey(key)) || (yMapping.ContainsKey(key)))
                {   

                    if (theCanvas.Dispatcher.CheckAccess())
                    {
                        this.color = generateNewColor();
                        blotches = new Blotches(this.theCanvas, this, key, pressure, xMapping[key], yMapping[key], this.color);
                    }

                    else
                    {
                        theCanvas.Dispatcher.Invoke(
                            System.Windows.Threading.DispatcherPriority.Normal,
                            new AnimateEllipseDrawn(this.DrawBlotch), key, pressure);
                    }
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.ToString());
            }
        }

        public Color generateNewColor()
        {
            Random rand = new Random();
            byte[] colorBytes = new byte[3];

            rand.NextBytes(colorBytes);
            return Color.FromArgb(50, colorBytes[0], colorBytes[1], colorBytes[2]);
        }

        public float getPressure()
        {
            return this.pressure;
        }

        public void setPressure(float pressure)
        {
            this.pressure = pressure;
        }

        public string getKey()
        {
            return this.key;
        }

        public void setKey(string key)
        {
            this.key = key;
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            Random random = new Random(); //generate random number
            PSK.Key key = new PSK.Key();

            key.Name = "Key" + e.Key.ToString();
            key.Pressure = random.Next(255);
            key.Text = e.Key.ToString();

            this.pressKeyboard.OnKeyPressed(new KeyPressedEventArgs(key));
        }
    }
}
